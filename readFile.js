// Synchronous way to read an encoded file
const fs = require("fs");
const text = fs.readFileSync("./coreModules.js", "UTF-8");
console.log(text);

// Asynchronous way to read an encoded file
const text2 = fs.readFile("./coreModules.js", "UTF-8", (err, text) => {
    console.log("File contents read: ");
    console.log(text);
})

// Asynchronous way to read a binary file
const text3 = fs.readFile("./coreModules.js", (err, text) => {
    if(err) {
        console.log(`An error has occurred: ${err.message}`);
        process.exit();
    }
    
    console.log("File contents read: ");
    console.log(text);
})