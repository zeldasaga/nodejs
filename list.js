// List the files and folders in a directory
const fs = require("fs");
const files = fs.readdirSync("./www/files");
console.log(`sychronous files: ${files}`);

const files1 = fs.readdir("./", (err, files) => {
    if (err) {
        throw err;
    }
    console.log("completed async file read");
    console.log(`asynch files: ${files}`);
});